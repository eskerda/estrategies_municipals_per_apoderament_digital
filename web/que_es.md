---
layout: page
permalink: /que_es/
toc: false
---

## La sobirania tecnològica a les polítiques públiques locals
Estratègies per l'apoderament digital als municipis és un document que pretén avançar en la Sobirania Tecnològica a través de la implementació de polítiques municipals concretes. La voluntat d'aquest document és el d'impregnar de sobirania tecnològica els programes dels partits polítics que concorreran a les eleccions municipals del 2019, no només dedicant un apartat especific dels seus programes a les propostes que farem a continuació, si no aplicant al programa sencer una nova mirada.

Tot i la clara aposta d'aquesta guia per un canvi en les polítiques municipals, el procés per avançar cap a aquesta sobirania, no s'ha d'entendre com un procés exclusivament liderat per l'administració pública, sinó com un fet compartit entre els diferents agents d'un territori. És per això que entenem aquesta guia com un full de ruta que ens ha de servir a les diferents entitats que treballem aquest tema, per poder fer un seguiment i revisió de la nostra pròpia feina.


## La tecnologia que ens envolta
La tecnologia, o l'activitat tecnològica d'una regió, influeix en el progrés econòmic i social d'aquesta.  És per això que calen unes polítiques municipals que donin als municipis la capacitat de ser protagonistes de la seva activitat tecnològica i que no es limitin, com passa en l'actualitat, només a potenciar l'evolució tecnològica orientada per l'interès privat de les empreses líders del sector. Aquest protagonisme ha de servir per fer créixer una tecnologia oberta, de fàcil accés i sota control democràtic.

A més, la tecnologia ha inundat tots els aspectes de la vida diària de les persones, facilitant o limitant les llibertats personals. Per això. cal que l'administració no se'n desentengui i no només reguli adequadament la l'us de les tecnologies en aquests àmbits, sinó que també sigui exemplar vetllant per les llibertats dels ciutadans.

No s'ha de confondre el fet de ser sobirans tecnològicament, amb una administració pública que controli la tecnologia, sinó tot el contrari. La sobirania tecnològica, s'ha de basar en tecnologies lliures: de lliure accés, ús, estudi i rèplica, que evitin, precisament, el control tecnològic. A més a més, la naturalesa d'aquestes tecnologies, propicia que el seu disseny i desenvolupament es faci a través de xarxes d'agents on es poden combinar les capacitats tècniques amb el coneixement de les necessitats del territori, i on no recaigui únicament sobre l'administació pública la responsabilitat de fer evolucionar la tecnologia.
