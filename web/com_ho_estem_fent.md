---
layout: page
title: "Com ho estem fent"
permalink: /com_ho_estem_fent/
---
## El procés

La proposta neix de diverses entitats que, des de fa temps, estem treballant per promoure la sobirania tecnològica i que ara hem iniciat un procés per compartir la feina ja feta i consensuar objectius, establir línies d'actuació comunes i compartir eines i recursos. El resultat d'aquest procés són les Estratègies per a l'Apoderament Digital i les Mesures per a que els ajuntaments les puguin portar a terme.

La voluntat és poder seguir ampliant i millorant el document com si fos un programara informàtic. És per això que estem seguint una metodologia àmpliament utilitzada al món del programari lliure, que ens està permetent fer créixer aquest projecte col·lectivament. Aquí tens el nostre [repositori](https://gitlab.com/estrategies_per_apoderament_digital/estrategies_municipals_per_apoderament_digital).
Amb el temps, volem afegir-hi recursos i eines que facilitin la posada en pràctica de les mesures. Tot això, mirant de fer arribar les estratègies no només a l'administració local, sino també a les entitats socials i a la ciutadania.

Es tracta d'un espai i un procés obert a la participació de noves persones i entitats, amb ganes de bolcar-hi les seves idees i aprenentatges. Per a més informació, escriviu a sobiraniamunicipal@bcnfs.org.

## Les entitats

Les entitats impulsores som:

  * Sobtec
  * Barcelona Free Software
  * Pangea
  * Colectic
  * Pirates de Catalunya
  * Xarxa Innovació Pública
  * GuifiNet/Exo

Les entitats contribuïdores són aquelles que fan propostes específiques per ampliar els diferents apartats de les estratègies. Trobareu més informació sobre com contribuir al document [COM_CONTRIBUIR](https://gitlab.com/estrategies_per_apoderament_digital/estrategies_municipals_per_apoderament_digital/blob/master/COM_CONTRIBUIR.md)

Finalment, també comptem amb entitats adherides, que són aquelles que donen suport a la proposta i a les mesures, i col·laboren per a la seva difusió. Contacteu-nos per a les adhesions.
