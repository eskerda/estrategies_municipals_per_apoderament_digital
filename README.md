# Introducció

La Guia per l'apoderament tecnològic als municipis és un document que pretén avançar en la Sobirania Tecnològica a través de la implementació de polítiques municipals concretes. La voluntat d'aquest document és el d'impregnar de sobirania tecnològica els programes dels partits polítics que concorreran a les **eleccions municipals del 2019**.

La Guia es divideix en dos documents estretament relacionats:
1. [**Les propostes programàtiques**](./la_guia)
   On hi ha un argumentari i les propostes per als programes polítics.

2. [**L'Annex**](./annex/ANNEX.md)
   On hi ha una explicació més extensa de cada proposta, experiències concretes, bibliografia, i consells d'implementació.

Els dos document es divideix en els següents apartats:

* [Infraestructura de telecomunicacions](./annex/INFRAESTRUCTURA_DE_TELECOMUNICACIONS.md)
* [Programari Lliure](./annex/PROGRAMARI_LLIURE.md)
* [Estàndars lliures](./annex/ESTANDARS_LLIURES.md)
* [Política de dades](./annex/POLÍTICA_DE_DADES.md)
* [Democratització de la tecnologia](./annex/DEMOCRATITZACIÓ_DE_LA_TECNOLOGIA.md)
* [Compra pública de dispositius electrònics i circularitat](./annex/COMPRA_PUBLICA_DE_DISPOSITIUS_ELECTRONICS_I_CIRCULARITAT.md)

### Entitats participants

La Guia és un projecte col·laboratiu que estem duent a terme diverses entitats i persones que treballem diàriament, i des de diferents àmbits, per avançar en la sobirania tecnològica.

Pots trobar més informació en el document [ENTITATS_PARTICIPANTS](ENTITATS_PARTICIPANTS.md)

## Com contribuir

Cadascun dels apartats anteriors es troba en un fitxer en format [Markdown (GitLab flavored)](https://docs.gitlab.com/ee/user/markdown.html). Per fer qualsevol aportació als documents cal que facis un [Merge request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html). També tens la opció d'enviar els teus suggeriments a sobiraniamunicipal@bcnfs.org.

Totes les aportacions seran debatudes conjuntament pels impulsors de La Guia i els redactors de l'aportació.
Tothom qui hagi fet una aportació al document pot demanar d'aparèixer en l'apartat d'entitats participants com a contribuidor.

Abans de fer una contribució llegeix el document [COM_CONTRIBUIR](COM_CONTRIBUIR.md).
